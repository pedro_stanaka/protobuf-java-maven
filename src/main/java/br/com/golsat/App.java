package br.com.golsat;

import br.com.golsat.proto.PersonOuterClass.Person;

/**
 * Hello world!
 */
public class App {
  public static void main(String[] args) {

    Person john = Person
        .newBuilder()
        .setId(123)
        .setName("John Doe")
        .addPhone(
            Person
                .PhoneNumber
                .newBuilder()
                .setNumber("551-1415")
        )
        .build();

    System.out.println(john);

  }
}
