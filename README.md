# Exemplo de Protobuf com Maven

Para rodar este exemplo, basta ter o maven instalado e configurado no PATH.

## Compilando e gerando as classes Java

mvn -T1C install

## Rodando o exemplo

java -jar target/protobuf-example-1.0-SNAPSHOT.jar


